export interface IContact {
  id: string;
  firstName: string;
  lastName: string;
  email: string;
  gdprAccepted?: boolean;
}

export class Contact implements IContact {
  id: string;
  firstName: string;
  lastName: string;
  email: string;
  gdprAccepted?;

  constructor(data: IContact) {

    if (typeof data.id === 'string') {
      console.error('Trying to create Contact with id as a string');
    }
    this.id           = data.id;
    this.firstName    = data.firstName;
    this.lastName     = data.lastName;
    this.email        = data.email;
    this.gdprAccepted = data.gdprAccepted;
  }
}

