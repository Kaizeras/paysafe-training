import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from "@angular/router";


const routes = [
  {path: '', redirectTo: 'contacts', pathMatch: 'full'},
  {path: 'contacts', loadChildren: 'src/app/contacts/contacts.module#ContactsModule'},
  {path: 'about', loadChildren: 'src/app/about/about.module#AboutModule'},
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes),
    CommonModule
  ],
  declarations: [],
  exports: [RouterModule],
})
export class AppRoutingModule {
}


