import { Directive, ElementRef, HostListener, Input, OnInit, Renderer2 } from '@angular/core';

@Directive({
  selector: '[appHighlight]',
})
export class HighlightDirective implements OnInit {
  @Input() highlightColor: string;

  @HostListener('mouseenter')
  onMouseEnter() {
    this.highlight(true);
  }

  @HostListener('mouseleave')
  onMouseLeave() {
    this.highlight(false);
  }

  ngOnInit() {
    if (!this.highlightColor) {
      this.highlightColor = 'yellow';
    }
  }

  constructor(private hostElement: ElementRef, private renderer: Renderer2) {
  }

  highlight(shouldHighLight: boolean) {


    if (shouldHighLight) {
      this.renderer.setStyle(this.hostElement.nativeElement, 'backgroundColor', this.highlightColor);
    } else {
      this.renderer.setStyle(this.hostElement.nativeElement, 'backgroundColor', '#fff');
    }


  }

}
