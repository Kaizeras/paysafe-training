import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MyUpperPipe } from "./my-upper.pipe";
import { HighlightDirective } from "./highlight.directive";

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [MyUpperPipe, HighlightDirective],
  exports: [MyUpperPipe, HighlightDirective]
})
export class WidgetModule {
}
