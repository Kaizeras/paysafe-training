import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'myUpper'
})
export class MyUpperPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    return value.toUpperCase() ;
  }

}
