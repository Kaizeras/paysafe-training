import { Component, OnInit, ViewChild } from '@angular/core';
import { Observable, Subscription } from "rxjs";
import { FormControl, FormGroup, NgForm, Validators } from "@angular/forms";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styles: [
    "ul { margin-left: 10px; background: #eee; padding: 15px; }",
    "li {display: inline-block;}",
    "li~li:before { content: '|'; margin: 0 7px 0 5px; }",
    ".active, .active a { color: #c40030; }"]
})
export class AppComponent implements OnInit {
  time = ''
  sub: Subscription
  @ViewChild('form') form: NgForm;

  contactForm:FormGroup


  ngOnInit() {
    this.time = Observable.create((observer) => {
      setInterval(() => {
        observer.next(new Date());
      }, 1000);
    });

    this.contactForm = new FormGroup({
      firstName: new FormControl('', Validators.required),
      lastName: new FormControl('', Validators.required),
      email: new FormControl('', [Validators.required, Validators.email])
    });
  }


}
