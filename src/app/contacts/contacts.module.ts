import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ContactListComponent } from "./contact-list/contact-list.component";
import { ContactDetailsComponent } from "./contact-details/contact-details.component";
import { ContactService } from "../contact.service";
import { WidgetModule } from "../widget/widget.module";
import { RouterModule } from "@angular/router";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { ContactRoutingModule } from "./contact-routing.module";

@NgModule({
  imports: [
    CommonModule,
    ContactRoutingModule,
    WidgetModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule
  ],
  declarations: [ContactListComponent, ContactDetailsComponent],
  providers: [ContactService],

})
export class ContactsModule {
}
