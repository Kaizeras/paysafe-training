import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute } from "@angular/router";
import { Contact } from "../../contact";
import { ContactService } from "../../contact.service";

@Component({
  selector: 'app-contact-details',
  templateUrl: './contact-details.component.html',
  styleUrls: ['./contact-details.component.css']

})
export class ContactDetailsComponent implements OnInit {
  selectedContact: Contact;
  contactForm: FormGroup;
  showEdit = false;

  constructor(private contactService: ContactService, private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.contactForm = new FormGroup({
      firstName: new FormControl('', Validators.required),
      lastName: new FormControl('', Validators.required),
      email: new FormControl('', [Validators.required, Validators.email])
    });

    this.route.params.subscribe((params) => {
      const selectedContactId = params['id'];
      this.contactService.getById(selectedContactId).subscribe((fetchedContact) => {
        this.selectedContact = fetchedContact;
      });
    });
  }

  onSubmit() {
    if (!this.contactForm.valid) {
      return;
    }

    if (this.selectedContact.id == null) {
      const newContact = this.contactForm.value;
      this.contactService.add(newContact).subscribe((addedContact: Contact) => {
        this.contactForm.reset();
        this.showEdit        = false;
        this.selectedContact = null;
      });

    } else {
      let editedContact = this.contactForm.value;
      editedContact.id  = this.selectedContact.id;
      this.contactService.update(editedContact).subscribe((updatedContact: Contact) => {
        this.contactForm.reset();
        this.showEdit = false;
      });
    }

  }

}
