import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from "@angular/router";
import { ContactListComponent } from "./contact-list/contact-list.component";
import { ContactDetailsComponent } from "./contact-details/contact-details.component";

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild([{
      path: '', component: ContactListComponent, children: [
        {path: ':id', component: ContactDetailsComponent},
      ]
    }])
  ],
  declarations: [],
  exports: [RouterModule]
})
export class ContactRoutingModule {
}
