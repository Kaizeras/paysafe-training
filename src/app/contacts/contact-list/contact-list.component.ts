import { Component, OnInit } from '@angular/core';

import { Router } from "@angular/router";
import { Contact, IContact } from "../../contact";
import { ContactService } from "../../contact.service";

@Component({
  selector: 'app-contact-list',
  templateUrl: './contact-list.component.html',
  styleUrls: ['./contact-list.component.css']
})
export class ContactListComponent implements OnInit {


  currentContact: Contact;
  contacts: Contact[];

  constructor(private contactService: ContactService, private router: Router) {
  }

  ngOnInit() {
    this.getAll();
  }


  getAll() {
    this.contactService.getAll().subscribe((contacts: Contact[]) => {
      this.contacts = contacts;
    });
  }

  selectContact(contact: IContact) {
    this.router.navigate([`contacts/${contact.id}`]);
  }

  removeContact(contact: IContact) {
    this.contactService.remove(contact).subscribe(() => {
      this.getAll();
    });
  }

}
