import { Injectable } from '@angular/core';
import { Contact, IContact } from "./contact";
import { HttpClient } from "@angular/common/http";
import { environment } from "../environments/environment";
import { map } from "rxjs/operators";
import { Observable } from "rxjs/index";

@Injectable({
  providedIn: 'root'
})
export class ContactService {

  contactsUrl = `${environment.apiUrl}contacts`;

  constructor(private http: HttpClient) {
  }

  getAll() {
    return this.http.get(this.contactsUrl).pipe(map(
      (contacts: Contact[]) => {
        return contacts.map((contact) => {
          return new Contact(contact);
        });
      }));
  }

  getById(contactId: string): Observable<any> {
    return this.http.get(`${this.contactsUrl}/${contactId}`);
  }

  add(contact: IContact) {
    // contact.id = ContactService._contactId++;
    // this.CONTACTS.push(contact);
    // return contact;

    return this.http.post(this.contactsUrl, contact);
  }

  remove(contact: IContact) {
    return this.http.delete(`${this.contactsUrl}/${contact.id}`);

  }

  update(contact: IContact) {
    return this.http.put(`${this.contactsUrl}/${contact.id}`, contact);

  }
}
